package com.eppinger.posterappbackend.exceptions.model

class UserNotFoundException(identification: String) : Exception("User $identification could not be found")
