package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.bson.types.ObjectId

class RefreshTokenType {

    @JsonIgnore
    public lateinit var _id: ObjectId

    @JsonProperty("refresh_token")
    public lateinit var refreshToken: String

    @JsonIgnore
    public lateinit var associatedUser: String

    @JsonIgnore
    public var valid: Boolean = false
}
