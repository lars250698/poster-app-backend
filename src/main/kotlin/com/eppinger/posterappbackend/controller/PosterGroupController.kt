package com.eppinger.posterappbackend.controller

import com.eppinger.posterappbackend.exceptions.controller.IllegalOperationException
import com.eppinger.posterappbackend.model.PosterGroupResponseType
import com.eppinger.posterappbackend.model.PosterGroupType
import com.eppinger.posterappbackend.model.PosterType
import com.eppinger.posterappbackend.model.UserDBType
import com.eppinger.posterappbackend.service.PosterService
import com.eppinger.posterappbackend.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/auth/poster-group")
class PosterGroupController {

    @Autowired
    private lateinit var posterService: PosterService

    @Autowired
    private lateinit var userService: UserService

    @GetMapping("/{id}")
    fun getGroup(@PathVariable(name = "id") id: String, @RequestAttribute(name = "user") user: UserDBType): ResponseEntity<PosterGroupResponseType> {
        val group = posterService.getPosterGroup(id)
        group.verifyAccess(user)
        val result = PosterGroupResponseType.from(group)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @GetMapping("/")
    fun getGroups(@RequestAttribute(name = "user") user: UserDBType): ResponseEntity<List<PosterGroupType>> {
        val posters = posterService.getPosterGroupsForUser(user)
        return ResponseEntity(posters, HttpStatus.OK)
    }

    @PostMapping("/")
    fun newGroup(@RequestBody posterGroupType: PosterGroupType, @RequestAttribute(name = "user") user: UserDBType) {
        posterService.addPosterGroup(posterGroupType, user)
    }

    @PostMapping("/{id}")
    fun updatePosterGroup(@PathVariable(name = "id") id: String, @RequestBody posterGroupType: PosterGroupType, @RequestAttribute(name = "user") user: UserDBType) {
        val group = posterService.getPosterGroup(id)
        group.updateWith(posterGroupType)
        posterService.updatePosterGroup(group)
    }

    @PostMapping("/{id}/poster")
    fun addPoster(@PathVariable(name = "id") id: String, @RequestBody poster: PosterType, @RequestAttribute(name = "user") user: UserDBType) {
        val group = posterService.getPosterGroup(id)
        group.verifyAccess(user)
        group.posters.add(poster)
        posterService.updatePosterGroup(group)
    }

    @DeleteMapping("/{id}/poster")
    fun removePoster(@PathVariable(name = "id") id: String, @RequestBody poster: PosterType, @RequestAttribute(name = "user") user: UserDBType) {
        val group = posterService.getPosterGroup(id)
        group.verifyAccess(user)
        group.posters.remove(poster)
        posterService.updatePosterGroup(group)
    }

    @PostMapping("/{id}/user")
    fun addUser(@PathVariable(name = "id") id: String, @RequestParam(name = "username", required = true) username: String, @RequestAttribute(name = "user") user: UserDBType) {
        val group = posterService.getPosterGroup(id)
        group.verifyAccess(user)
        val newUser = userService.getUserByName(username)
        group.users.add(newUser._id.toHexString())
        posterService.updatePosterGroup(group)
    }

    @DeleteMapping("/{id}/user")
    fun removeUser(@PathVariable(name = "id") id: String, @RequestParam(name = "username", required = true) username: String, @RequestAttribute(name = "user") user: UserDBType) {
        val group = posterService.getPosterGroup(id)
        group.verifyAccess(user)
        val removeUser = userService.getUserByName(username)
        if (user._id == removeUser._id) {
            throw IllegalOperationException()
        }
        group.users.remove(removeUser._id.toHexString())
        posterService.updatePosterGroup(group)
    }
}
