package com.eppinger.posterappbackend.service

import com.eppinger.posterappbackend.exceptions.model.EntityNotFoundException
import com.eppinger.posterappbackend.model.PosterGroupDBType
import com.eppinger.posterappbackend.model.PosterGroupType
import com.eppinger.posterappbackend.model.RefreshTokenType
import com.eppinger.posterappbackend.model.UserDBType
import com.mongodb.BasicDBObject
import com.mongodb.MongoClient
import com.mongodb.MongoClientOptions
import org.bson.BSON
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.pojo.PojoCodecProvider
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class DatabaseService(
        @param:Value("\${database.mongo.database.name}") val dbName: String,
        @param:Value("\${database.mongo.collections.posters}") val posterCollectionName: String,
        @param:Value("\${database.mongo.collections.users}") val userCollectionName: String,
        @param:Value("\${database.mongo.collections.tokens}") val tokenCollectionName: String
) {
    private val registry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
            fromProviders(PojoCodecProvider.builder().automatic(true).build()))
    private val options = MongoClientOptions.builder()
            .codecRegistry(registry)
            .build()
    private val client = MongoClient("localhost", options)
    private val posterDb = client.getDatabase(dbName)

    public val posterCollection = posterDb.getCollection(posterCollectionName, PosterGroupDBType::class.java)
    public val userCollection = posterDb.getCollection(userCollectionName, UserDBType::class.java)
    public val tokenCollection = posterDb.getCollection(tokenCollectionName, RefreshTokenType::class.java)

    public fun getObjectID(id: String): ObjectId {
        val objId: ObjectId
        try {
            objId = ObjectId(id)
        } catch (e: IllegalArgumentException) {
            throw EntityNotFoundException()
        }
        return objId
    }

    public fun queryById(id: ObjectId): BasicDBObject {
        val query = BasicDBObject()
        query["_id"] = id.toHexString()
        return query
    }

    public fun queryById(id: String): BasicDBObject {
        val query = BasicDBObject()
        query["_id"] = id
        return query
    }
}
