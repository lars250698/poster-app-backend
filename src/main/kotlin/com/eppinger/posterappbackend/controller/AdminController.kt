package com.eppinger.posterappbackend.controller

import com.eppinger.posterappbackend.model.UserRequestType
import com.eppinger.posterappbackend.model.UserResponseType
import com.eppinger.posterappbackend.model.UserType
import com.eppinger.posterappbackend.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/auth/admin")
class AdminController {

    @Autowired
    private lateinit var userService: UserService

    @PostMapping("/user")
    public fun addUser(@RequestBody user: UserType) {
        userService.addUser(user)
    }

    @PostMapping("/user/{id}")
    public fun modifyUser(@PathVariable("id") id: String, @RequestBody userRequestType: UserRequestType) {
        TODO("implement")
    }

    @DeleteMapping("/user/{id}")
    public fun deleteUser(@PathVariable("id") id: String) {
        userService.deleteUser(id)
    }

    @GetMapping("/user")
    public fun getUsers(): ResponseEntity<List<UserResponseType>> {
        TODO("implement")
    }

    @GetMapping("/user/{id}")
    public fun getUser(@PathVariable("id") id: String): ResponseEntity<UserResponseType> {
        TODO("implement")
    }
}
