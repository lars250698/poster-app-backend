package com.eppinger.posterappbackend.model

import org.bson.types.ObjectId
import org.springframework.security.crypto.bcrypt.BCrypt

class UserDBType: UserType() {

    public lateinit var _id: ObjectId

    companion object {
        public fun from(userType: UserType): UserDBType {
            val userDatabaseType = UserDBType()
            userDatabaseType.name = userType.name
            userDatabaseType.admin = userType.admin
            userDatabaseType.passwordHash = userType.passwordHash
            return userDatabaseType
        }

        public fun from(userRequestType: UserRequestType): UserDBType {
            val userDatabaseType = UserDBType()
            userDatabaseType.name = userRequestType.name
            userDatabaseType.admin = userRequestType.admin
            userDatabaseType.passwordHash = BCrypt.hashpw(userRequestType.password, BCrypt.gensalt(10))
            return userDatabaseType
        }
    }
}
