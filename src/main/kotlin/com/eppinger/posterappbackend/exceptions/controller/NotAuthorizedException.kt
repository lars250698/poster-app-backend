package com.eppinger.posterappbackend.exceptions.controller

class NotAuthorizedException : Exception("User is not authorized")
