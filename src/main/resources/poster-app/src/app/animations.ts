import { AnimationTriggerMetadata, trigger, style, animate, transition } from '@angular/animations';


export class Animations {
    public static popInAnimation: AnimationTriggerMetadata = trigger(
      'popInAnimation',
      [
        transition(
          ':enter', [
            style({ transform: 'scale(0)', 'opacity': 0 }),
            animate('200ms', style({ transform: 'scale(1)', 'opacity': 1 })),
          ]
        ),
        transition(
          ':leave', [
            style({ transform: 'scale(1)', 'opacity': 1 }),
            animate('200ms', style({ transform: 'scale(0)', 'opacity': 0 })),
          ]
        )
      ]
    )
}
