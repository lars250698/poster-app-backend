package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonProperty

class UserRequestType: UserType() {

    @JsonProperty("password")
    public lateinit var password: String
}
