package com.eppinger.posterappbackend.exceptions.controller

class AuthorizationRequiredException : Exception("Authorization Required")
