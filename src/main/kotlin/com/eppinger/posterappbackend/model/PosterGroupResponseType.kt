package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonProperty

class PosterGroupResponseType: PosterGroupType() {

    @JsonProperty("id")
    public lateinit var id: String

    companion object {
        public fun from(posterGroupDBType: PosterGroupDBType): PosterGroupResponseType {
            val posterGroupResponseType = PosterGroupResponseType()
            posterGroupResponseType.id = posterGroupDBType._id.toHexString()
            posterGroupResponseType.title = posterGroupDBType.title
            posterGroupResponseType.created = posterGroupDBType.created
            posterGroupResponseType.posters = posterGroupDBType.posters
            posterGroupResponseType.users = posterGroupDBType.users
            posterGroupResponseType.default = posterGroupDBType.default
            return posterGroupResponseType
        }
    }
}
