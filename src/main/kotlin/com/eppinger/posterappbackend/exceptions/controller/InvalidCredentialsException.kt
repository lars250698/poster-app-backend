package com.eppinger.posterappbackend.exceptions.controller

class InvalidCredentialsException: Exception("Invalid credentials")
