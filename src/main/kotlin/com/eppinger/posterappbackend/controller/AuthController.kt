package com.eppinger.posterappbackend.controller

import com.eppinger.posterappbackend.model.AccessTokenType
import com.eppinger.posterappbackend.model.RefreshTokenType
import com.eppinger.posterappbackend.model.UserDBType
import com.eppinger.posterappbackend.service.TokenService
import com.eppinger.posterappbackend.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1")
class AuthController {

    private val LOG = LoggerFactory.getLogger(AuthController::class.java)

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var tokenService: TokenService

    @GetMapping("/login")
    fun login(@RequestParam(name = "username") username: String, @RequestParam(name = "password") password: String): ResponseEntity<RefreshTokenType> {
        val user = userService.getUserByName(username)
        user.verifyPassword(password)
        tokenService.invalidatePreviousTokens(user)
        val resp = tokenService.generateRefreshToken(user)
        return ResponseEntity(resp, HttpStatus.OK)
    }

    @GetMapping("/auth/logout")
    fun logout(@RequestAttribute(name = "user") user: UserDBType) {
        tokenService.invalidatePreviousTokens(user)
    }

    @PostMapping("/auth_token")
    fun refresh(@RequestBody token: RefreshTokenType): ResponseEntity<AccessTokenType> {
        val jwt = tokenService.verifyRefreshToken(token.refreshToken)
        val userId = jwt.getClaim("sub").asString()
        val user = userService.getUserById(userId)
        val resp = tokenService.generateAccessToken(user)
        return ResponseEntity(resp, HttpStatus.OK)
    }
}
