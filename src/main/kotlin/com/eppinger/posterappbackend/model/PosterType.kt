package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class PosterType {

    @JsonProperty("location")
    public lateinit var location: LocationType

    @JsonProperty("picture_url")
    public lateinit var pictureUrl: String

    @JsonProperty("comment")
    public lateinit var comment: String

    @JsonProperty("added")
    public var added: Long = -1L

    @JsonProperty("remove_by")
    public var removeBy: Long = -1L

}
