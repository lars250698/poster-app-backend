package com.eppinger.posterappbackend.interceptor

import com.auth0.jwt.exceptions.JWTVerificationException
import com.eppinger.posterappbackend.exceptions.controller.AuthorizationRequiredException
import com.eppinger.posterappbackend.service.TokenService
import com.eppinger.posterappbackend.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtInterceptor : HandlerInterceptor {

    private val LOG = LoggerFactory.getLogger(JwtInterceptor::class.java)

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var tokenService: TokenService

    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val token: String = tokenService.getTokenFromRequest(request)
        try {
            val jwt = tokenService.verifyAccessToken(token)
            val userId = jwt.getClaim("sub").asString()
            val user = userService.getUserById(userId)
            request.setAttribute("user", user)
            request.setAttribute("decodedJwt", jwt)
        } catch (e: JWTVerificationException) {
            LOG.debug("Dropping request, couldn't verify token")
            throw AuthorizationRequiredException()
        }
        return true
    }
}
