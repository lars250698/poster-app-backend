package com.eppinger.posterappbackend.controller

import com.eppinger.posterappbackend.exceptions.controller.*
import com.eppinger.posterappbackend.exceptions.model.EntityNotFoundException
import com.eppinger.posterappbackend.exceptions.model.UserNotFoundException
import com.eppinger.posterappbackend.model.ErrorType
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.lang.RuntimeException

@ControllerAdvice
class ExceptionHandlerController {

    private val LOG = LoggerFactory.getLogger(ExceptionHandler::class.java)

    private fun errorStatus(status: HttpStatus, reason: String): ResponseEntity<ErrorType> {
        val err = ErrorType(status, reason)
        return ResponseEntity(err, status)
    }

    @ExceptionHandler(AuthorizationRequiredException::class)
    public fun authorizationRequired(): ResponseEntity<ErrorType> {
        return errorStatus(HttpStatus.UNAUTHORIZED, "Authorization required")
    }

    @ExceptionHandler(InvalidCredentialsException::class)
    public fun invalidCredentials(): ResponseEntity<ErrorType> {
        return errorStatus(HttpStatus.UNAUTHORIZED, "Invalid credentials")
    }

    @ExceptionHandler(NotAuthorizedException::class)
    public fun notAuthorized(): ResponseEntity<ErrorType> {
        return errorStatus(HttpStatus.FORBIDDEN, "Forbidden")
    }

    @ExceptionHandler(RuntimeException::class)
    public fun internalErrorFatal(e: RuntimeException): ResponseEntity<ErrorType> {
        LOG.warn("Handling fatal internal exception: {}", e)
        return errorStatus(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error")
    }

    @ExceptionHandler(InternalException::class)
    public fun internalError(): ResponseEntity<ErrorType> {
        LOG.warn("Handling non-fatal internal exception")
        return errorStatus(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error")
    }

    @ExceptionHandler(EntityNotFoundException::class)
    public fun entityNotFound(): ResponseEntity<ErrorType> {
        return errorStatus(HttpStatus.BAD_REQUEST, "Requested entity could not be found")
    }

    @ExceptionHandler(UserNotFoundException::class)
    public fun userNotFound(): ResponseEntity<ErrorType> {
        return errorStatus(HttpStatus.BAD_REQUEST, "User could not be found")
    }

    @ExceptionHandler(IllegalOperationException::class)
    public fun illegalOperation(): ResponseEntity<ErrorType> {
        return errorStatus(HttpStatus.BAD_REQUEST, "Illegal operation")
    }
}
