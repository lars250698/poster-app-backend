package com.eppinger.posterappbackend.service

import com.eppinger.posterappbackend.exceptions.model.EntityNotFoundException
import com.eppinger.posterappbackend.model.*
import com.mongodb.BasicDBObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PosterService {

    @Autowired
    private lateinit var db: DatabaseService

    public fun getPosterGroup(id: String): PosterGroupDBType {
        val query = BasicDBObject()
        query["_id"] = db.getObjectID(id)
        return db.posterCollection.find(query).first() ?: throw EntityNotFoundException()
    }

    public fun getPosterGroupsForUser(user: UserDBType): List<PosterGroupResponseType> {
        val query = BasicDBObject()
        query["users"] = user._id.toHexString()
        return db.posterCollection
                .find(query)
                .into(mutableListOf())
                .map { p -> PosterGroupResponseType.from(p) }
    }

    public fun addPosterGroup(posterGroupType: PosterGroupType, user: UserDBType) {
        posterGroupType.verifyIntegrity()
        val posterGroupDatabaseType = PosterGroupDBType.from(posterGroupType)
        posterGroupDatabaseType.users.add(user._id.toHexString())
        db.posterCollection.insertOne(posterGroupDatabaseType)
    }

    public fun updatePosterGroup(posterGroupDBType: PosterGroupDBType) {
        val query = BasicDBObject()
        query["_id"] = posterGroupDBType._id
        db.posterCollection.findOneAndReplace(query, posterGroupDBType)
    }

}
