import { Component } from '@angular/core';
import { RouterModule, Routes, RouterOutlet } from '@angular/router'
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'poster-app';
}
