package com.eppinger.posterappbackend.service

import com.eppinger.posterappbackend.exceptions.controller.InternalException
import com.eppinger.posterappbackend.exceptions.model.UserNotFoundException
import com.eppinger.posterappbackend.model.UserDBType
import com.eppinger.posterappbackend.model.UserType
import com.mongodb.BasicDBObject
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest

@Service
class UserService {

    @Autowired
    private lateinit var db: DatabaseService

    fun getUserByName(username: String): UserDBType {
        val query = BasicDBObject()
        query["name"] = username
        return db.userCollection.find(query).first() ?: throw UserNotFoundException(username)
    }

    fun getUserById(id: String): UserDBType {
        val query = BasicDBObject()
        query["_id"] = db.getObjectID(id)
        return db.userCollection.find(query).first() ?: throw UserNotFoundException(id)
    }

    fun addUser(user: UserType) {
        val newUser = UserDBType.from(user)
        db.userCollection.insertOne(newUser)
    }

    fun deleteUser(id: String) {
        val query = BasicDBObject()
        query["_id"] = db.getObjectID(id)
        db.userCollection.findOneAndDelete(query)
    }

}
