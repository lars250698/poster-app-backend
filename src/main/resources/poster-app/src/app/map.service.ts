import { Injectable } from '@angular/core';
import { LatLngExpression } from 'leaflet';
import { LocationService } from './location.service';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  public location: LatLngExpression = [49.3987524, 8.6724335];
  public zoom: number = 13;

  constructor(
    private locationService: LocationService
  ) {
    this.locationService.getLocation()
      .then(location => {
        this.location = location;
      })
  }

  centerOnCurrentLocation() {
    this.locationService.getLocation()
      .then(location => {
        this.location = location;
      })
      .catch(error => {
        // handle
      })
  }
}
