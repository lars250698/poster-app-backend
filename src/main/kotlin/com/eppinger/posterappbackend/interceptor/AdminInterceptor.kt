package com.eppinger.posterappbackend.interceptor

import com.auth0.jwt.exceptions.JWTVerificationException
import com.eppinger.posterappbackend.exceptions.controller.NotAuthorizedException
import com.eppinger.posterappbackend.exceptions.model.UserNotFoundException
import com.eppinger.posterappbackend.model.UserType
import com.eppinger.posterappbackend.service.TokenService
import com.eppinger.posterappbackend.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import java.lang.Exception
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AdminInterceptor : HandlerInterceptor {

    private val LOG = LoggerFactory.getLogger(AdminInterceptor::class.java)

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var tokenService: TokenService

    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val token = tokenService.getTokenFromRequest(request)
        try {
            val jwt = tokenService.verifyAccessToken(token)
            val id = jwt.getClaim("sub").asString()
            val user = userService.getUserById(id)
            return user.admin
        } catch (e: JWTVerificationException) {
            LOG.debug("Dropping request, couldn't verify admin status")
            throw NotAuthorizedException()
        }
    }
}
