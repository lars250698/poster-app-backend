package com.eppinger.posterappbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PosterAppBackendApplication

fun main(args: Array<String>) {
    runApplication<PosterAppBackendApplication>(*args)
}
