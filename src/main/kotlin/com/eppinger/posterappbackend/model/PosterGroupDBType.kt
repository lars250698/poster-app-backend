package com.eppinger.posterappbackend.model

import org.bson.types.ObjectId

class PosterGroupDBType: PosterGroupType() {

    public lateinit var _id: ObjectId

    companion object {
        public fun from(posterGroupType: PosterGroupType): PosterGroupDBType {
            val posterGroupDatabaseType = PosterGroupDBType()
            posterGroupDatabaseType.title = posterGroupType.title
            posterGroupDatabaseType.created = posterGroupType.created
            posterGroupDatabaseType.posters = posterGroupType.posters
            posterGroupDatabaseType.users = posterGroupType.users
            posterGroupDatabaseType.default = posterGroupType.default
            return posterGroupDatabaseType
        }
    }
}
