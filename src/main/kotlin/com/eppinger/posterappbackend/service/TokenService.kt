package com.eppinger.posterappbackend.service

import com.auth0.jwt.JWT
import com.auth0.jwt.exceptions.JWTCreationException
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.DecodedJWT
import com.eppinger.posterappbackend.exceptions.controller.AuthorizationRequiredException
import com.eppinger.posterappbackend.exceptions.controller.InternalException
import com.eppinger.posterappbackend.exceptions.controller.NotAuthorizedException
import com.eppinger.posterappbackend.model.AccessTokenType
import com.eppinger.posterappbackend.model.RefreshTokenType
import com.eppinger.posterappbackend.model.UserDBType
import com.eppinger.posterappbackend.utils.CryptUtils
import com.mongodb.BasicDBObject
import org.bson.Document
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*
import javax.servlet.http.HttpServletRequest

@Service
class TokenService(
        @param:Value("\${authentication.jwt.issuer}") val issuer: String,
        @param:Value("\${authentication.jwt.audience}") val audience: String,
        @param:Value("\${authentication.jwt.auth.expiration.minutes:5}") val accessExpires: Int,
        @param:Value("\${authentication.jwt.refresh.expiration.hours:48}") val refreshExpires: Int
) {

    private val LOG = LoggerFactory.getLogger(TokenService::class.java)

    @Autowired
    private lateinit var cryptUtils: CryptUtils

    @Autowired
    private lateinit var db: DatabaseService

    public fun generateRefreshToken(user: UserDBType): RefreshTokenType {
        val refreshToken = RefreshTokenType()
        refreshToken.associatedUser = user._id.toHexString()
        try {
            val token = JWT.create()
                    .withIssuer(issuer)
                    .withAudience(audience)
                    .withIssuedAt(Date())
                    .withClaim("purpose", "refresh")
                    .withClaim("sub", user._id.toHexString())
                    .withClaim("name", user.name)
                    .withClaim("admin", user.admin)
                    .sign(cryptUtils.algorithm)
            refreshToken.refreshToken = token
            refreshToken.valid = true
            db.tokenCollection.insertOne(refreshToken)
        } catch (e: JWTCreationException) {
            LOG.error("Failed to create refresh token for user {}", user.name)
            throw InternalException("Failed to create token")
        }
        return refreshToken
    }

    public fun generateAccessToken(user: UserDBType): AccessTokenType {
        val accessToken = AccessTokenType()
        val exp = getTimeIn(Calendar.MINUTE, accessExpires)
        try {
            val token = JWT.create()
                    .withIssuer(issuer)
                    .withAudience(audience)
                    .withIssuedAt(Date())
                    .withExpiresAt(exp)
                    .withClaim("purpose", "access")
                    .withClaim("sub", user._id.toHexString())
                    .withClaim("name", user.name)
                    .withClaim("admin", user.admin)
                    .sign(cryptUtils.algorithm)
            accessToken.accessToken = token
        } catch (e: JWTCreationException) {
            LOG.error("Failed to create access token for user {}", user.name)
            throw InternalException("Failed to create token")
        }
        return accessToken
    }

    public fun verifyRefreshToken(token: String): DecodedJWT {
        val query = Document("refreshToken", token)
        val dbToken = db.tokenCollection.find(query).first()
        val exp = getTimeIn(Calendar.HOUR, refreshExpires)
        if (dbToken?.valid != true) {
            throw NotAuthorizedException()
        }
        try {
            val verifier = JWT.require(cryptUtils.algorithm)
                    .withIssuer(issuer)
                    .withAudience(audience)
                    .withClaim("purpose", "refresh")
                    .build()
            return verifier.verify(token)
        } catch (e: JWTVerificationException) {
            val update = Document("\$set", Document("valid", false))
            db.tokenCollection.updateMany(query, update)
            throw NotAuthorizedException()
        }
    }

    public fun verifyAccessToken(token: String): DecodedJWT {
        val verifier = JWT.require(cryptUtils.algorithm)
                .withIssuer(issuer)
                .withAudience(audience)
                .withClaim("purpose", "access")
                .build()
        return verifier.verify(token)
    }

    public fun getTokenFromRequest(request: HttpServletRequest): String {
        return request.getHeader("Authorization")?.replace("Bearer ", "") ?: throw AuthorizationRequiredException()
    }

    public fun invalidatePreviousTokens(user: UserDBType) {
        val query = BasicDBObject()
        query["associatedUser"] = user._id.toHexString()
        query["valid"] = true
        val update = Document("\$set", Document("valid", false))
        db.tokenCollection.updateMany(query, update)
    }

    private fun getTimeIn(unit: Int, amount: Int): Date {
        val c = Calendar.getInstance()
        c.time = Date()
        c.add(unit, amount)
        return c.time
    }
}
