package com.eppinger.posterappbackend.utils

import org.springframework.stereotype.Component
import java.net.URL

@Component
class ResourceUtils {

    public fun getResource(path: String): URL {
        return this::class.java.classLoader.getResource(path)
    }
}
