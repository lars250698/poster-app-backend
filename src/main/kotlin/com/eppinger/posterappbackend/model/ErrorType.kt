package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.http.HttpStatus
import java.text.SimpleDateFormat
import java.util.*

class ErrorType(
        _status: HttpStatus,
        _reason: String
) {
    @JsonProperty("status")
    public val status = _status

    @JsonProperty("reason")
    public val reason = _reason

    @JsonProperty("time")
    public val time = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(Date())
}
