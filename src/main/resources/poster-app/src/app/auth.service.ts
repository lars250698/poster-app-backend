import { Injectable } from '@angular/core';
import { User } from './user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwtHelper = new JwtHelperService();
  public user = User.from(localStorage.token);

  constructor(
    private router: Router,
    private http: HttpClient
  ) { }

  getToken(): Observable<any> {
    return this.http.post('/v1/auth_token', {refresh_token: sessionStorage.refreshToken})
      /*.subscribe((data: any) => {
        localStorage.token = data.access_token;
      })*/
  }

  setToken(data: any) {
    localStorage.token = data.access_token;
  }

  isTokenValid() {
    try {
      return !this.jwtHelper.isTokenExpired(localStorage.token);
    } catch (_) {
      return false;
    }
  }

  logIn() {
    this.user = User.from(sessionStorage.refreshToken);
    console.log(this.user)
  }

  logOut() {
    this.user = null;
  }

  redirectToLogOut(returnRoute?: string) {
    this.redirectWithReturn("/logout", returnRoute);
  }

  redirectToLogIn(returnRoute?: string) {
    this.redirectWithReturn("/", returnRoute);
  }

  private redirectWithReturn(redirect: string, returnRoute?: string) {
    let params = {};
    if (returnRoute) {
      params = {
        queryParams: {
          return: returnRoute
        }
      }
    }
    this.router.navigate([redirect], params);
  }
}
