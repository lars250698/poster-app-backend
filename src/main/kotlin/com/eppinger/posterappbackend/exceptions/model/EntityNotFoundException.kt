package com.eppinger.posterappbackend.exceptions.model

import java.lang.Exception

class EntityNotFoundException: Exception()
