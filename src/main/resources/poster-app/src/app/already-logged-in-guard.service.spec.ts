import { TestBed } from '@angular/core/testing';

import { AlreadyLoggedInGuardService } from './already-logged-in-guard.service';

describe('AlreadyLoggedInGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlreadyLoggedInGuardService = TestBed.get(AlreadyLoggedInGuardService);
    expect(service).toBeTruthy();
  });
});
