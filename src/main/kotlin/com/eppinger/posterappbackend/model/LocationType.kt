package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonProperty

class LocationType {

    @JsonProperty("latitiude")
    public var latitiude: Double = 0.0

    @JsonProperty("longitude")
    public var longitude: Double = 0.0
}
