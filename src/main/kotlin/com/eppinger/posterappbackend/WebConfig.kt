package com.eppinger.posterappbackend

import com.eppinger.posterappbackend.interceptor.AdminInterceptor
import com.eppinger.posterappbackend.interceptor.JwtInterceptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class WebConfig: WebMvcConfigurer {

    @Autowired
    private lateinit var jwtInterceptor: JwtInterceptor

    @Autowired
    private lateinit var adminInterceptor: AdminInterceptor

    public override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(jwtInterceptor).addPathPatterns("/*/auth/**")
        registry.addInterceptor(adminInterceptor).addPathPatterns("/*/auth/admin/**")
    }
}
