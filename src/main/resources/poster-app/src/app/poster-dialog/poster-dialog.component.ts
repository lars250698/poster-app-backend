import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Poster } from '../poster';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { LatLngExpression } from 'leaflet';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-poster-dialog',
  templateUrl: './poster-dialog.component.html',
  styleUrls: ['./poster-dialog.component.scss']
})
export class PosterDialogComponent implements OnInit {
  @Output() newPoster = new EventEmitter<Poster>();
  private poster: Poster;
  private loading: boolean = false;
  private error: String;

  public modalRef: BsModalRef

  constructor(
    private modalService: BsModalService,
    private locationService: LocationService
  ) { }

  ngOnInit() {
  }

  newPosterDialogOpen(template: Template) {
    this.poster = new Poster();
    this.loading = true;
    this.locationService.getLocation()
      .then(location => {
        this.poster.location = location;
        this.loading = false;
        this.modalRef = this.modalService.show(template);
      })
      .catch(error => {
        // handle
      });
  }

  close() {
    this.modalRef.hide();
  }

  confirm() {
    this.newPoster.emit(this.poster);
    this.close();
  }
}
