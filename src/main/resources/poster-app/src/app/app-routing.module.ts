import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapViewComponent } from './map-view/map-view.component';
import { ListViewComponent } from './list-view/list-view.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AccountViewComponent } from './account-view/account-view.component';
import { AboutComponent } from './about/about.component';

import { UserAuthenticatedGuardService } from './user-authenticated-guard.service';
import { AdminGuardService } from './admin-guard.service';
import { AlreadyLoggedInGuardService } from './already-logged-in-guard.service';
import { from } from 'rxjs';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [AlreadyLoggedInGuardService] },
  { path: 'signup', component: SignUpComponent, canActivate: [AlreadyLoggedInGuardService] },
  { path: 'map', component: MapViewComponent, canActivate: [UserAuthenticatedGuardService] },
  { path: 'list', component: ListViewComponent, canActivate: [UserAuthenticatedGuardService] },
  { path: 'user/:id', component: AccountViewComponent, canActivate: [UserAuthenticatedGuardService] },
  { path: 'admin', component: AdminViewComponent, canActivate: [UserAuthenticatedGuardService, AdminGuardService] },
  { path: 'about', component: AboutComponent },
  { path: 'logout', component: LogoutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
