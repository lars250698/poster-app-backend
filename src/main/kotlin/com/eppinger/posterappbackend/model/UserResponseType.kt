package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonProperty

class UserResponseType: UserType() {

    @JsonProperty("id")
    public lateinit var id: String

    companion object {
        public fun from(userDBType: UserDBType): UserResponseType {
            val userResponseType = UserResponseType()
            userResponseType.id = userDBType._id.toHexString()
            userResponseType.name = userDBType.name
            userResponseType.admin = userDBType.admin
            userResponseType.passwordHash = userDBType.passwordHash
            return userResponseType
        }
    }
}
