import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthService} from '../auth.service';
import {HttpClient} from '@angular/common/http';
import {Animations} from '../animations';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [Animations.popInAnimation]
})
export class LoginComponent implements OnInit {

  private returnUrl: string;
  private username: string;
  private password: string;
  private error: string;
  private errorTimeout;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['return'] || '/map';
  }

  logIn() {
    this.http.get('/v1/login', {
      params: {
        password: this.password,
        username: this.username
      }
    }).subscribe((data: any) => {
      sessionStorage.refreshToken = data.refresh_token;
      // this.setUnloadListener();
      this.authService.logIn();
      this.authService.getToken()
        .subscribe((data: any) => {
          this.authService.setToken(data);
          this.router.navigate([this.returnUrl]);
        });
    }, (error) => {
      if (error.error instanceof ErrorEvent) {
        console.error('An error occured: ' + error.error.message);
        this.setError('An error occured');
      } else {
        this.setError('Invalid username or password!');
      }
    })
  }

  setUnloadListener() {
    addEventListener('beforeunload', () => {
      this.http.get('/v1/logout')
    })
  }

  setError(message: string) {
    this.error = message;
    this.errorTimeout = setTimeout(() => this.error = null, 3000);
    this.username = '';
    this.password = '';
  }

  closeError() {
    this.error = null;
    clearTimeout(this.errorTimeout);
  }

}
