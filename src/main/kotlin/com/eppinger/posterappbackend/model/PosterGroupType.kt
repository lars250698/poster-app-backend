package com.eppinger.posterappbackend.model

import com.eppinger.posterappbackend.exceptions.controller.NotAuthorizedException
import com.eppinger.posterappbackend.exceptions.model.IncompleteTypeException
import com.fasterxml.jackson.annotation.JsonProperty

open class PosterGroupType {

    @JsonProperty("title")
    public lateinit var title: String

    @JsonProperty("created")
    public var created: Long = -1L

    @JsonProperty("users")
    public lateinit var users: MutableList<String>

    @JsonProperty("posters")
    public lateinit var posters: MutableList<PosterType>

    @JsonProperty("default")
    public var default: Boolean = false

    public fun userCanAccess(user: UserDBType): Boolean {
        return users.contains(user._id.toHexString())
    }

    public fun verifyAccess(user: UserDBType) {
        if (!userCanAccess(user)) {
            throw NotAuthorizedException()
        }
    }

    public fun verifyIntegrity() {
        if (this::users.isInitialized) users = mutableListOf()
        if (this::posters.isInitialized) posters = mutableListOf()
        if (!(this::title.isInitialized && this.created > -1L)) {
            throw IncompleteTypeException()
        }
    }

    public fun updateWith(posterGroupType: PosterGroupType) {
        if (posterGroupType::title.isInitialized) title = posterGroupType.title
    }

}
