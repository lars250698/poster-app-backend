package com.eppinger.posterappbackend.model

import com.eppinger.posterappbackend.exceptions.controller.InvalidCredentialsException
import com.fasterxml.jackson.annotation.JsonProperty
import org.bson.types.ObjectId
import org.springframework.security.crypto.bcrypt.BCrypt

open class UserType {

    @JsonProperty("name")
    public lateinit var name: String

    @JsonProperty("admin")
    public var admin: Boolean = false

    public lateinit var passwordHash: String

    public fun passwordCorrect(clearTextPassword: String): Boolean {
        return BCrypt.checkpw(clearTextPassword, passwordHash)
    }

    public fun verifyPassword(clearTextPassword: String) {
        if (!passwordCorrect(clearTextPassword)) {
            throw InvalidCredentialsException()
        }
    }
}
