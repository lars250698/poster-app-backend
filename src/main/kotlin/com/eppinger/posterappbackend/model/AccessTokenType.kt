package com.eppinger.posterappbackend.model

import com.fasterxml.jackson.annotation.JsonProperty

class AccessTokenType {

    @JsonProperty("access_token")
    public lateinit var accessToken: String
}
