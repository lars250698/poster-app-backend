package com.eppinger.posterappbackend.utils

import com.auth0.jwt.algorithms.Algorithm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.rmi.server.RMIClassLoader.getClassLoader
import java.security.KeyFactory
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*
import java.util.regex.Pattern

@Component
class CryptUtils(
        @param:Value("\${utils.crypt.publickey.path:static/certs/cert.pem}") val pubKeyPath: String,
        @param:Value("\${utils.crypt.privatekey.path:static/certs/key.pem}") val privKeyPath: String,
        @param:Autowired val resourceUtils: ResourceUtils
) {

    private val pubKey: RSAPublicKey = getPubKey(pubKeyPath)
    private val privKey: RSAPrivateKey = getPrivKey(privKeyPath)

    public val algorithm: Algorithm = Algorithm.RSA256(pubKey, privKey)

    private fun getPubKey(name: String): RSAPublicKey {
        val bytes = resourceUtils.getResource(name).readBytes()
        val spec = X509EncodedKeySpec(bytes)
        val kf = KeyFactory.getInstance("RSA")
        return kf.generatePublic(spec) as RSAPublicKey
    }

    private fun getPrivKey(name: String): RSAPrivateKey {
        val bytes = resourceUtils.getResource(name).readBytes()
        val spec = PKCS8EncodedKeySpec(bytes)
        val kf = KeyFactory.getInstance("RSA")
        return kf.generatePrivate(spec) as RSAPrivateKey
    }

}
